import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-base-widget',
  template: `
  <div class="container" *ngFor="let dataCarrier of dataLoader">
  <div class="row">
    <div class="col-xs-6">
      <div class="col-md-4">
        <img src="{{dataCarrier.image}}" class="img-responsive"/>
      </div>
      <div class="col-md-4"><h1>{{dataCarrier.name}}</h1>
      <a href="http://twitter.com">{{dataCarrier.link}}</a><br>
      Follower {{dataCarrier.friend}}<br>
      Following 464<br>
      Tweets {{dataCarrier.count}}
      </div>
    </div> 
  </div>
  <div class="row">
    <div class="container">
      <div class="col-xs-6">
        <h4><b>Location</b> : {{dataCarrier.location}}</h4>
        <h4><b>Web</b> : <a href="{{dataCarrier.web}}">{{dataCarrier.web}}</a></h4>
      </div>
    </div>
  </div>
</div>`
})
export class BaseWidgetSocmedDetailComponent implements OnInit {

  public dataLoader: Array<any> = [{"name": "Kaka","image": "../../assets/twitter.png","count": "234","web": "http://google.com",	"link": "track",	"friend": "234.234",	"location": "parigi baru"}];

  constructor() {console.log(this.dataLoader);}
  
  ngOnInit() { 
  }

}
