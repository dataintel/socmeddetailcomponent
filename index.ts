import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetSocmedDetailComponent } from './src/base-widget.component';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
	  BaseWidgetSocmedDetailComponent
  ],
  exports: [
    BaseWidgetSocmedDetailComponent
  ]
})
export class SocmedDetailModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SocmedDetailModule,
      providers: []
    };
  }
}
